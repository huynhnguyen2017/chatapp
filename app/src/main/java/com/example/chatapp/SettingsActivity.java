package com.example.chatapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettingsActivity extends AppCompatActivity
{

    private CheckBox favoriteFootball;
    private CheckBox favoriteTV;
    private CheckBox favoriteTravel;

    private RadioButton male, female;


    private Button UpdateAccountSettings;
    private EditText userName, userStatus;
    private CircleImageView userProfileImage;

    private String currentUserID;
    private FirebaseAuth mAuth;
    private DatabaseReference RootRef;

    private StorageReference UserProfileImagesRef;
    private ProgressDialog loadingBar;
    private ImageView backImage;

    private static final int GalleryPick = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mAuth = FirebaseAuth.getInstance();
        currentUserID = mAuth.getCurrentUser().getUid();
        RootRef = FirebaseDatabase.getInstance().getReference();
        UserProfileImagesRef = FirebaseStorage.getInstance().getReference().child("Profile Images");

        InitializeFields();

        // userName.setVisibility(View.INVISIBLE);

        UpdateAccountSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateSettings();
            }
        });


        RetrieveUserInfo();

        // back to home page
        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendUserToMainActivity();
            }
        });

        userProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent galleryIntent = new Intent();
                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, GalleryPick);
            }
        });
    }




    private void InitializeFields()
    {
        UpdateAccountSettings = (Button) findViewById(R.id.update_settings_button);
        userName = (EditText) findViewById(R.id.set_user_name);
        userStatus = (EditText) findViewById(R.id.set_profile_status);

        userProfileImage = (CircleImageView) findViewById(R.id.set_profile_image);
        loadingBar = new ProgressDialog(this);

        favoriteFootball = (CheckBox) findViewById(R.id.favoriteFootball);
        favoriteTV = (CheckBox) findViewById(R.id.favoriteTV);
        favoriteTravel = (CheckBox) findViewById(R.id.favoriteTravel);

        male = (RadioButton) findViewById(R.id.radMale);
        female = (RadioButton) findViewById(R.id.radFeMale);
        backImage = (ImageView) findViewById(R.id.image_back);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GalleryPick && resultCode == RESULT_OK && data != null)
        {
            Uri ImageUri = data.getData();

            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1, 1)
                    .start(this);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
        {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK)
            {

                loadingBar.setTitle("Set Profile Image");
                loadingBar.setMessage("Please wait, image is updating...");
                loadingBar.setCanceledOnTouchOutside(false);
                loadingBar.show();

                Uri resultUri = result.getUri();

                final StorageReference filepath = UserProfileImagesRef.child(currentUserID + ".jpg");


                filepath.putFile(resultUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        if (taskSnapshot.getMetadata() != null) {
                            if (taskSnapshot.getMetadata().getReference() != null) {
                                Task<Uri> result = taskSnapshot.getStorage().getDownloadUrl();
                                result.addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        String imageUrl = uri.toString();
                                        RootRef.child("Users").child(currentUserID).child("image")
                                                .setValue(imageUrl)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful())
                                                        {
                                                            Toast.makeText(SettingsActivity.this, "Image save in database successfully...",
                                                                    Toast.LENGTH_SHORT).show();
                                                            loadingBar.dismiss();
                                                        }
                                                        else
                                                        {
                                                            String message = task.getException().toString();
                                                            Toast.makeText(SettingsActivity.this, "Error: " + message,
                                                                    Toast.LENGTH_SHORT).show();
                                                            loadingBar.dismiss();
                                                        }
                                                    }
                                                });
                                        //createNewPost(imageUrl);
                                    }
                                });
                            }
                        }
                    }});
            }
        }
    }

    private void UpdateSettings()
    {
        String setUserName = userName.getText().toString();
        String setStatus = userStatus.getText().toString();
        String checkedMale = male.isChecked() ? "true" : "false";
        String checkedFeMale = female.isChecked() ? "true" : "false";
        String checkedFootball = favoriteFootball.isChecked() ? "true" : "false";
        String checkedTV = favoriteTV.isChecked() ? "true" : "false";
        String checkedTravel = favoriteTravel.isChecked() ? "true" : "false";

        if (TextUtils.isEmpty(setUserName))
        {
            Toast.makeText(SettingsActivity.this, "Please write your name...", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(setStatus))
        {
            Toast.makeText(SettingsActivity.this, "Please write your status...", Toast.LENGTH_SHORT).show();
        }
        else
        {
            HashMap<String, String> profileMap = new HashMap<>();
                profileMap.put("uid", currentUserID);
                profileMap.put("name", setUserName);
                profileMap.put("status", setStatus);
                profileMap.put("male", checkedMale);
                profileMap.put("female", checkedFeMale);
                profileMap.put("football", checkedFootball);
                profileMap.put("tv", checkedTV);
                profileMap.put("travel", checkedTravel);

            RootRef.child("Users").child(currentUserID).setValue(profileMap)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task)
                        {
                            if (task.isSuccessful())
                            {
//                                SendUserToMainActivity();
                                Toast.makeText(SettingsActivity.this, "Profile Updated Successfully...", Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                String message = task.getException().toString();
                                Toast.makeText(SettingsActivity.this, "Error " + message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }


    private void RetrieveUserInfo()
    {
        RootRef.child("Users").child(currentUserID)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                    {
                        if ((dataSnapshot.exists()) && (dataSnapshot.hasChild("name") &&
                                dataSnapshot.hasChild("image")))
                        {
                            String retrieveUserName = dataSnapshot.child("name").getValue().toString();
                            String retrieveStatus = dataSnapshot.child("status").getValue().toString();
                            String retrieveProfileImage = dataSnapshot.child("image").getValue().toString();
                            String retrieveMale = dataSnapshot.child("male").getValue().toString();
                            String retrieveFeMale = dataSnapshot.child("female").getValue().toString();
                            String retrieveFootball = dataSnapshot.child("football").getValue().toString();
                            String retrieveTravel = dataSnapshot.child("travel").getValue().toString();
                            String retrieveTV = dataSnapshot.child("tv").getValue().toString();
                            Boolean checkRetrieveMale = retrieveMale.equals("true");
                            Boolean checkRetrieveFeMale = retrieveFeMale.equals("true");
                            Boolean checkRetrieveFootball = retrieveFootball.equals("true");
                            Boolean checkRetrieveTravel = retrieveTravel.equals("true");
                            Boolean checkRetrieveTV = retrieveTV.equals("true");

                            userName.setText(retrieveUserName);
                            userStatus.setText(retrieveStatus);

                            male.setChecked(checkRetrieveMale);
                            female.setChecked(checkRetrieveFeMale);
                            favoriteFootball.setChecked(checkRetrieveFootball);
                            favoriteTravel.setChecked(checkRetrieveTravel);
                            favoriteTV.setChecked(checkRetrieveTV);

                            Picasso.get().load(retrieveProfileImage).into(userProfileImage);
                        }
                        else if ((dataSnapshot.exists()) && (dataSnapshot.hasChild("name")))
                        {
                            String retrieveUserName = dataSnapshot.child("name").getValue().toString();
                            String retrieveStatus = dataSnapshot.child("status").getValue().toString();

                            String retrieveMale = dataSnapshot.child("male").getValue().toString();
                            String retrieveFeMale = dataSnapshot.child("female").getValue().toString();
                            String retrieveFootball = dataSnapshot.child("football").getValue().toString();
                            String retrieveTravel = dataSnapshot.child("travel").getValue().toString();
                            String retrieveTV = dataSnapshot.child("tv").getValue().toString();
                            Boolean checkRetrieveMale = retrieveMale.equals("true");
                            Boolean checkRetrieveFeMale = retrieveFeMale.equals("true");
                            Boolean checkRetrieveFootball = retrieveFootball.equals("true");
                            Boolean checkRetrieveTravel = retrieveTravel.equals("true");
                            Boolean checkRetrieveTV = retrieveTV.equals("true");

                            userName.setText(retrieveUserName);
                            userStatus.setText(retrieveStatus);

                            male.setChecked(checkRetrieveMale);
                            female.setChecked(checkRetrieveFeMale);
                            favoriteFootball.setChecked(checkRetrieveFootball);
                            favoriteTravel.setChecked(checkRetrieveTravel);
                            favoriteTV.setChecked(checkRetrieveTV);
                        }
                        else
                        {
                            userName.setVisibility(View.VISIBLE);
                            Toast.makeText(SettingsActivity.this, "Please update your profile information...", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }


    private void SendUserToMainActivity()
    {
        Intent mainIntent = new Intent(SettingsActivity.this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
        finish();
    }



}
